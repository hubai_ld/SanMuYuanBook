(function ($) {
    $.getUrlParam = function (name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
        var r = window.location.search.substr(1).match(reg);
        if (r != null) return unescape(r[2]);
        return null;
    }
})(jQuery);

//写Cookie
function addCookie(objName, objValue, objHours) {
    var str = objName + "=" + escape(objValue); //编码
    if (objHours > 0) {//为0时不设定过期时间，浏览器关闭时cookie自动消失
        var date = new Date();
        var ms = objHours * 3600 * 1000;
        date.setTime(date.getTime() + ms);
        str += "; expires=" + date.toGMTString();
    }
    document.cookie = str;
}

//读Cookie
function getCookie(objName) {//获取指定名称的cookie的值
    var arrStr = document.cookie.split("; ");
    for (var i = 0; i < arrStr.length; i++) {
        var temp = arrStr[i].split("=");
        if (temp[0] == objName) return unescape(temp[1]);  //解码
    }
    return "";
}
$(function () {

    let color = getCookie("font_color");
    if(color!==""){
        $(".main-text-wrap").css("color",color);
    }
    let size = getCookie("backgroup_size");
    if(size!==""){
        $(".float-op-wrap").css("background-image", "url(../img/backgroup/"+size+".jpg)");
    }
    let item = getCookie("fontSize");
    if (item == null) {
        item = "20px";
        $("#size").html("20");
    }
    $("#size").html(item.replace("px", ""));
    $("#body").css("font-size", item);
    $.ajax({
        type: "POST",
        url: "/book/getBookChapter",
        data: {
            "bookCod": $.getUrlParam('bookCod'),
            "chapterCod": $.getUrlParam('chapterCod'),
            "dataSource": $.getUrlParam('dataSource')
        },
        dataType: "json",
        success: function (data) {
            $("#body").append(data.catalogueText);
            $("#head").append(data.catalogueName);
            if (data.upCode != null) {
                $("#j_chapterPrev").attr("href", "readBook.html?bookCod=" + data.bookCode + "&&chapterCod=" + data.upCode + "&&dataSource=" + $.getUrlParam('dataSource'));
            }
            if (data.nextCode != null) {
                $("#j_chapterNext").attr("href", "readBook.html?bookCod=" + data.bookCode + "&&chapterCod=" + data.nextCode + "&&dataSource=" + $.getUrlParam('dataSource'));
            }
        },
        complete: function (data) {

        }
    });
    $("#j_fontSize span").click(function () {
        var thisEle = $("#body").css("font-size");
        var textFontSize = parseFloat(thisEle, 10);
        var unit = thisEle.slice(-2); //获取单位
        var cName = $(this).attr("id");
        if (cName == "bigger") {
            if (textFontSize <= 30) {
                textFontSize += 1;
                addCookie("fontSize", textFontSize + unit,4);
                $("#size").html(textFontSize);
            }
        } else if (cName == "smaller") {
            if (textFontSize >= 12) {
                textFontSize -= 1;
                addCookie("fontSize", textFontSize + unit,4);
                $("#size").html(textFontSize);
            }
        }

        $("#body").css("font-size", textFontSize + unit);
    });
    $(".back").click(function () {
        window.location.href = "BookCatalogue.html?bookCod=" + $.getUrlParam('bookCod') + "&&dataSource=" + $.getUrlParam('dataSource');
    })
    $("#shezhi").click(function () {
        $("#j_setting").attr("style", "display: block;");
    })
    $("#guanbi").click(function () {
        $("#j_setting").attr("style", "display: none;");
    })
    $("#img_button").click(function () {
        let value = $("#back_img_name").val();
        addCookie("backgroup_size",value,4);
        $(".float-op-wrap").css("background-image", "url(../img/backgroup/"+value+".jpg)");

    })
    $("#color_button").click(function () {
        let value = $("#color").val();
        addCookie("font_color",value,4);
        $(".main-text-wrap").css("color",value);
    })
})


