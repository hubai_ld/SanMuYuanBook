(function ($) {
    $.getUrlParam = function (name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
        var r = window.location.search.substr(1).match(reg);
        if (r != null) return unescape(r[2]);
        return null;
    }
})(jQuery);
$(function () {

//写Cookie
    function addCookie(objName, objValue, objHours) {
        var str = objName + "=" + escape(objValue); //编码
        if (objHours > 0) {//为0时不设定过期时间，浏览器关闭时cookie自动消失
            var date = new Date();
            var ms = objHours * 3600 * 1000;
            date.setTime(date.getTime() + ms);
            str += "; expires=" + date.toGMTString();
        }
        document.cookie = str;
    }

//读Cookie
    function getCookie(objName) {//获取指定名称的cookie的值
        var arrStr = document.cookie.split("; ");
        for (var i = 0; i < arrStr.length; i++) {
            var temp = arrStr[i].split("=");
            if (temp[0] == objName) return unescape(temp[1]);  //解码
        }
        return "";
    }
    let size = getCookie("backgroup_size");
    if(size!==""){
        $(".float-op-wrap").css("background-image", "url(../img/backgroup/"+size+".jpg)");
    }
    $("#img_button").click(function () {
        let value = $("#back_img_name").val();
        addCookie("backgroup_size",value,4);
        $(".float-op-wrap").css("background-image", "url(../img/backgroup/"+value+".jpg)");

    })
    $("#submit").click(function () {
        $.ajax({
            type:"POST",
            url:"/book/replaceBackgroup",
            data:{
                "file":$("#pic").val()
            },
            dataType:"json",
            success:function (data) {
                alert(data)
            },complete:function(){
                alert("替换成功,项目重启后生效")
            }
        }
        );
    })
    $("#shezhi").click(function () {
        $("#j_setting").attr("style","display: block;");
    })
    $("#guanbi").click(function () {
        $("#j_setting").attr("style","display: none;");
    })
    $.ajax({
        type: "POST",
        url: "/cartoon/getCartoonChapter",
        data: {
            "cartoonCod": $.getUrlParam('cartoonCod'),
            "chapterCod": $.getUrlParam('chapterCod'),
            "dataSource": $.getUrlParam('dataSource')
        },
        dataType: "json",
        success: function (data) {
            for (let i = 0; i < data.catalogueSrc.length; i++) {
                $("#body").append("<img class='imgs' src='" + data.catalogueSrc[i] + "'/>");
            }
            $("#head").append(data.catalogueName);
            if (data.upCode != null) {
                $("#j_chapterPrev").attr("href", "readCartoon.html?cartoonCod=" + $.getUrlParam('cartoonCod') + "&&chapterCod=" + data.upCode+"&&dataSource="+$.getUrlParam('dataSource'));
            }
            $(".back").attr("href", "CartoonCatalogue.html?cartoonCod=" + $.getUrlParam('cartoonCod')+"&&dataSource="+$.getUrlParam('dataSource'))
            if (data.nextCode != null) {
                $("#j_chapterNext").attr("href", "readCartoon.html?cartoonCod=" + $.getUrlParam('cartoonCod') + "&&chapterCod=" + data.nextCode+"&&dataSource="+$.getUrlParam('dataSource'));
            }
        },
        complete: function (data) {

        }
    });

    function getPath(obj) {
        if (obj) {
            if (window.navigator.userAgent.indexOf("MSIE") >= 1) {
                obj.select();
                return document.selection.createRange().text;
            } else if (window.navigator.userAgent.indexOf("Firefox") >= 1) {
                if (obj.files) {
                    return obj.files.item(0).getAsDataURL();
                }
                return obj.value;
            }
            return obj.value;
        }
    }
})

