function show() {
    $('#list').empty();
    let key=$("#s-box").val();
    if(key===""&&$.cookie("key")!=null){
        key=$.cookie("key");
        $("#s-box").val(key);

    }else{
        $.cookie("key",$("#s-box").val());
    }
    $.ajax({
        type: "POST",
        url: "/book/getBookList",
        data: {
            "key":key,
            "dataSource":$("#dataSource").val()
        },
        dataType: "json",
        success: function (data) {
            if(data.length===0){
                $("#list").append("<p>。。。。。。。没找到这本书哟，换换数据源再找找叭！</p>");
                return
            }
            var strhtml="";
            for (let i = 0; i < data.length; i++) {
                strhtml+="<li class=\"res-book-item\" data-bid=\"1019664125\" data-rid=\"1\">\n" +
                "                            <div class=\"book-img-box\">\n" +
                "                                <a href=\"page/BookCatalogue.html?bookCod="+data[i].bookCode+"&&dataSource="+$("#dataSource").val()+"\" target=\"_blank\" data-eid=\"qd_S04\" data-algrid=\"0.0.0\" data-bid=\"1019664125\"><img src=\"img/not.jpg\"></a>\n" +
                "                            </div>\n" +
                "                            <div class=\"book-mid-info\">\n" +
                "                                \n" +
                "                                <h4><a href=\"page/BookCatalogue.html?bookCod="+data[i].bookCode+"&&dataSource="+$("#dataSource").val()+"\" target=\"_blank\" data-eid=\"qd_S05\" data-bid=\"1019664125\" data-algrid=\"0.0.0\"><cite class=\"red-kw\">"+data[i].bookName+"</cite></a></h4>\n" +
                "                                <p class=\"author\">\n" +
                "                                    <img src=\"img/user.f22d3.png\"><a class=\"name\" data-eid=\"qd_S06\" href=\"page/BookCatalogue.html?bookCod="+data[i].bookCode+"&&dataSource="+$("#dataSource").val()+"\" target=\"_blank\">"+data[i].bookAuthor+"</a> <em>|</em><a href=\"https://www.qidian.com/xianxia\" data-eid=\"qd_S07\" target=\"_blank\">仙侠</a><em>|</em><span>连载</span>\n" +
                "                                </p>\n" +
                "                                <p class=\"intro\">\n" +
                "                                  暂无简介\n" +
                "                                </p>\n" +
                "                                <p class=\"update\"><a href=\"page/BookCatalogue.html?bookCod="+data[i].bookCode+"&&dataSource="+$("#dataSource").val()+"\" target=\"_blank\" data-eid=\"qd_S08\" data-bid=\"1019664125\">——————————————————————————</a><em>·</em><span>15小时前</span>\n" +
                "                                </p>\n" +
                "                            </div>\n" +
                "                            <div class=\"book-right-info\">\n" +
                "                                <div class=\"total\">\n" +
                "                                    <p><span> 99999999万</span>总字数</p>\n" +
                "                                    <p><span> 99999999万</span>总推荐</p>\n" +
                "                                </div>\n" +
                "                                <p class=\"btn\">\n" +
                "                                    <a class=\"red-btn\" href=\"page/BookCatalogue.html?bookCod="+data[i].bookCode+"&&dataSource="+$("#dataSource").val()+"\" data-eid=\"qd_S02\" target=\"_blank\">书籍详情</a>\n" +
                "                                    \n" +
                "                                </p>\n" +
                "                            </div>\n" +
                "                        </li>";
            }
            $("#list").append(strhtml);
        }
    });
}
$(function () {
    // if($.cookie("keyPage")!=null){
    //     list($.cookie("keyPage"));
    // }else
        if($.cookie("key")!=null){
        show();
    }
})
