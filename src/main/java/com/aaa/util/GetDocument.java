package com.aaa.util;

import com.aaa.config.SSLHelper;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

/**
 * @author 杨森
 * @version 1.0
 * @Title:
 * @date 2020/8/25 13:11
 */
public class GetDocument {
    private GetDocument(){}
    public static Document getDocument(String url,String code1, String code2) {
        SSLHelper.init();
        if(!"".equals(code1)){
            url+= code1+ "/";
        }
        if(!"".equals(code2)){
            url+= code2+ ".html";
        }
        Document document = null;
        try {
            document = Jsoup.connect(url).timeout(5000).get();
        } catch (IOException e) {
            try {
                Thread.sleep(3000);
                document = Jsoup.connect(url).timeout(5000).get();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }

        return document;
    }
}
