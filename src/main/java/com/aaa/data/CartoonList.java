package com.aaa.data;

import com.aaa.config.SSLHelper;
import com.aaa.entity.Cartoon;
import com.aaa.util.GetDocument;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 杨森
 * @version 1.0
 * @Title:
 * @date 2020/8/25 13:23
 */
public class CartoonList {
    public static List<Cartoon> setDataSource(String key, String dataSource) {
        SSLHelper.init();
        if ("gufengmh8".equals(dataSource)||"36mh".equals(dataSource)||"wuxiamh".equals(dataSource)) {
            return gufengmh8(dataSource,key);
        }
        return null;
    }
    private static List<Cartoon> gufengmh8(String data,String key) {
        Document document = GetDocument.getDocument("https://m."+data+".com/search/?keywords=" + key, "", "");
        Elements itemBox = document.getElementsByClass("itemBox");
        List<Cartoon> list = new ArrayList<>(itemBox.size());
        itemBox.forEach(element -> {
            Cartoon cartoon = new Cartoon();
            Elements itemTxt = element.getElementsByClass("itemTxt");
            //获取漫画名
            String name = itemTxt.get(0).childNode(1).childNode(0).toString();
            //作者
            String author = itemTxt.get(0).childNode(3).childNode(0).toString();
            //封面
            String imgSrc = element.childNode(1).childNode(1).childNode(0).attr("src");
            String s = itemTxt.get(0).childNode(1).attr("href");
            String substring1 = s.substring(0, s.length() - 1);
            String substring = substring1.substring(substring1.lastIndexOf("/") + 1);
            cartoon.setcCode(substring);
            cartoon.setcName(name);
            cartoon.setcAuthor(author);
            cartoon.setcImgSrc(imgSrc);
            list.add(cartoon);
        });
        return list;
    }
}
