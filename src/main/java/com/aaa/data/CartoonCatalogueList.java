package com.aaa.data;

import com.aaa.config.SSLHelper;
import com.aaa.dto.CartoonCatalogueDto;
import com.aaa.entity.CartoonCatalogue;
import com.aaa.util.GetDocument;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 杨森
 * @version 1.0
 * @Title:
 * @date 2020/8/25 15:11
 */
public class CartoonCatalogueList {
    public static CartoonCatalogueDto setDataSource(String cartoonCod, String dataSource) {
        if ("gufengmh8".equals(dataSource)||"36mh".equals(dataSource)||"wuxiamh".equals(dataSource)) {
            return gufengmh8(dataSource,cartoonCod);
        }
        return null;
    }
    private static CartoonCatalogueDto gufengmh8(String data,String cartoonCod) {
        CartoonCatalogueDto cartoonCatalogueDtos = new CartoonCatalogueDto();
        Document document = GetDocument.getDocument("https://m."+data+".com/manhua/", cartoonCod, "");
        Element elementById = document.getElementsByClass("list").get(0);
        Elements intro = document.getElementsByClass("txtDesc autoHeight");
        Element cover = document.getElementById("Cover");
        Elements lis = elementById.getElementsByTag("li");
        Elements name = document.getElementsByTag("h1");
        List<CartoonCatalogue> cartoonCatalogues = new ArrayList<>(lis.size());
        for (int i = 0; i < lis.size(); i++) {
            Element li = lis.get(i);
            com.aaa.entity.CartoonCatalogue cartoonCatalogue = new com.aaa.entity.CartoonCatalogue();
            if(li.childNodeSize()<2){
                continue;
            }
            String href = li.childNode(1).attr("href");
            String cCode = href.substring(href.lastIndexOf("/") + 1, href.lastIndexOf("."));
            cartoonCatalogue.setCatalogueCode(Integer.valueOf(cCode));
            String cataName = li.childNode(1).childNode(1).childNode(0).toString();
            cartoonCatalogue.setCatalogueName(cataName);
            if (i != 0) {
                if(lis.get(i - 1).childNodeSize()<2){
                    cartoonCatalogue.setUpCode(null);
                }else{
                    String href1 = lis.get(i - 1).childNode(1).attr("href");
                    String upCode = href1.substring(href1.lastIndexOf("/") + 1, href1.lastIndexOf("."));
                    cartoonCatalogue.setUpCode(Integer.valueOf(upCode));
                }
            }
            if (i != lis.size() - 1) {
                String href1 = lis.get(i + 1).childNode(1).attr("href");
                String nextCode = href1.substring(href1.lastIndexOf("/") + 1, href1.lastIndexOf("."));
                cartoonCatalogue.setNextCode(Integer.valueOf(nextCode));
            }
            cartoonCatalogues.add(cartoonCatalogue);
        }
        Elements pic = document.getElementsByClass("pic");
        String author = pic.get(0).childNode(5).childNode(3).childNode(0).toString();
        cartoonCatalogueDtos.setCartoonAuthor(author);
        cartoonCatalogueDtos.setCartoonName(name.get(0).text());
        cartoonCatalogueDtos.setCartoonCode(cartoonCod);
        cartoonCatalogueDtos.setCartoonIntro(intro.get(0).childNode(0).toString().replace("简介：", ""));
        cartoonCatalogueDtos.setCartoonImage(cover.childNode(1).attr("src"));
        cartoonCatalogueDtos.setCartoonCatalogues(cartoonCatalogues);
        return cartoonCatalogueDtos;
    }
}
