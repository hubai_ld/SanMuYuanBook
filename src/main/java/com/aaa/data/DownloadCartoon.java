package com.aaa.data;

import com.aaa.config.SSLHelper;
import com.aaa.dto.CartoonCatalogueDto;
import com.aaa.entity.CartoonCatalogue;
import com.aaa.service.CartoonService;
import com.aaa.service.impl.BookServiceImpl;
import com.aaa.util.DataProcessing;
import com.aaa.util.GetDocument;
import com.aaa.util.ZipUtils;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static com.aaa.util.DataProcessing.splitList;

/**
 * @author 杨森
 * @version 1.0
 * @Title:
 * @date 2020/9/3 9:29
 */
public class DownloadCartoon {
    private static CartoonService cartoonService;

    private static final int COUNT = 2;

    public static void setDataSource(String cartoonCod, String cartoonName, String dataSource, ExecutorService executorService, CartoonService cartoonService) {
        DownloadCartoon.cartoonService = cartoonService;
        if ("gufengmh8".equals(dataSource) || "36mh".equals(dataSource)||"wuxiamh".equals(dataSource)) {
            try {
                gufengmh8(dataSource, cartoonCod, cartoonName, executorService);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static void gufengmh8(String dataSource, String cartoonCod, String cartoonName, ExecutorService executorService) throws IOException, InterruptedException {
        CartoonCatalogueDto cartoonCatalogueDto = CartoonCatalogueList.setDataSource(cartoonCod, dataSource);
        assert cartoonCatalogueDto != null;
        List<CartoonCatalogue> cartoonCatalogues = cartoonCatalogueDto.getCartoonCatalogues();
        for (CartoonCatalogue cartoonCatalogue : cartoonCatalogues) {
            CartoonCatalogue cartoonCatalogue1 = CartoonChapter.setDataSource(cartoonCod, cartoonCatalogue.getCatalogueCode().toString(), dataSource, cartoonService);
            assert cartoonCatalogue1 != null;
            List<String> catalogueSrc = cartoonCatalogue1.getCatalogueSrc();
            try {
                download(cartoonName, cartoonCatalogue, catalogueSrc);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        FileOutputStream fos = new FileOutputStream("/usr/local/webapps/file/" + cartoonName + ".zip");
        ZipUtils.toZip("/usr/local/webapps/file/" + cartoonName, fos, true);
        File file = new File("/usr/local/webapps/file/" + cartoonName);
        deleteFile(file);
    }

    private static void deleteFile(File file) {
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            for (int i = 0; i < files.length; i++) {
                deleteFile(files[i]);
            }
        }
        file.delete();
    }

    private static void download(String cartoonName, CartoonCatalogue
            cartoonCatalogue, List<String> catalogueSrc) {
        int count = catalogueSrc.size();
        for (int i = 0; i < count; i++) {
            String catalogueName = cartoonCatalogue.getCatalogueName();
            String path = "/usr/local/webapps/file/" + cartoonName + "/" + catalogueName + "/" + i + ".jpg";
            File file = new File(path);
            if (!file.exists()) {
                System.out.println(catalogueName + "--" + i + ".jpg" + "开始下载");
                String src = catalogueSrc.get(i);
                Connection.Response response = null;
                try {
                    response = Jsoup.connect(src).timeout(60000).ignoreContentType(true).execute();
                } catch (Exception e) {
                    try {
                        Thread.sleep(3000);
                        response = Jsoup.connect(src).timeout(60000).ignoreContentType(true).execute();
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
                byte[] bytes = response.bodyAsBytes();
                File parentFile = file.getParentFile();
                if (!parentFile.exists()) {
                    parentFile.mkdirs();
                }
                BufferedOutputStream bos = null;
                FileOutputStream fos = null;
                try {
                    file.createNewFile();
                    fos = new FileOutputStream(file);
                    bos = new BufferedOutputStream(fos);
                    bos.write(bytes);
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    try {
                        bos.close();
                        fos.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            }

        }
    }
}
