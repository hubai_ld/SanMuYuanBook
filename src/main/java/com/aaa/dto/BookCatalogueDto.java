package com.aaa.dto;


import com.aaa.entity.BookCatalogue;

/**
 * @author 杨森
 * @version 1.0
 * @Title: BookCatalogue
 * @date 2020/7/28 14:56
 */
public class BookCatalogueDto {
    /**
     * 书名
     */
    private String bookName;
    /**
     * 章节id
     */
    private Integer catalogueId;
    /**
     * 作者
     */
    private String bookAuthor;

    /**
     * 章节名称
     */
    private String catalogueName;

    /**
     * 章节编码
     */
    private Integer catalogueCode;
    /**
     * 下一章code
     */
    private Integer nextCode;

    /**
     * 上一章code
     */
    private Integer upCode;

    /**
     * 书籍编码
     */
    private String bookCode;

    /**
     * 书籍图片
     */
    private String bookImage;

    /**
     * 书籍简介
     */
    private String bookIntro;

    /**
     * 章节内容
     */
    private String catalogueText;


    public BookCatalogueDto(){}
    public BookCatalogueDto(BookCatalogue bookCatalogue){
        this.catalogueText=bookCatalogue.getCatalogueText();
        this.catalogueCode=bookCatalogue.getCatalogueCode();
        this.catalogueName=bookCatalogue.getCatalogueName();
        this.nextCode=bookCatalogue.getNextCode();
    }

    public Integer getCatalogueCode() {
        return catalogueCode;
    }

    public void setCatalogueCode(Integer catalogueCode) {
        this.catalogueCode = catalogueCode;
    }

    public Integer getUpCode() {
        return upCode;
    }

    public void setUpCode(Integer upCode) {
        this.upCode = upCode;
    }

    public Integer getCatalogueId() {
        return catalogueId;
    }

    public void setCatalogueId(Integer catalogueId) {
        this.catalogueId = catalogueId;
    }

    public String getBookAuthor() {
        return bookAuthor;
    }

    public void setBookAuthor(String bookAuthor) {
        this.bookAuthor = bookAuthor;
    }

    public Integer getNextCode() {
        return nextCode;
    }

    public void setNextCode(Integer nextCode) {
        this.nextCode = nextCode;
    }

    public String getBookCode() {
        return bookCode;
    }

    public void setBookCode(String bookCode) {
        this.bookCode = bookCode;
    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getCatalogueName() {
        return catalogueName;
    }

    public void setCatalogueName(String catalogueName) {
        this.catalogueName = catalogueName;
    }

    public Integer getCatalogueCod() {
        return catalogueCode;
    }

    public void setCatalogueCod(Integer catalogueCod) {
        this.catalogueCode = catalogueCod;
    }

    public String getBookCod() {
        return bookCode;
    }

    public void setBookCod(String bookCod) {
        this.bookCode = bookCod;
    }

    public String getBookImage() {
        return bookImage;
    }

    public void setBookImage(String bookImage) {
        this.bookImage = bookImage;
    }

    public String getBookIntro() {
        return bookIntro;
    }

    public void setBookIntro(String bookIntro) {
        this.bookIntro = bookIntro;
    }

    public String getCatalogueText() {
        return catalogueText;
    }

    public void setCatalogueText(String catalogueText) {
        this.catalogueText = catalogueText;
    }
}
