package com.aaa.controller;

import com.aaa.dto.BookCatalogueDto;
import com.aaa.entity.Book;
import com.aaa.entity.FileBook;
import com.aaa.service.BookService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

import static com.aaa.util.Download.download;

/**
 * @author 杨森
 * @version 1.0
 * @Title: 书籍操作
 * @date 2020/7/24 15:53
 */
@Controller
@Api(value = "书籍管理")
@RequestMapping("/book")
public class BookController {

    @Autowired
    private BookService bookService;

    private final static Logger log = LoggerFactory.getLogger(BookController.class);

    @PostMapping("/getBookList")
    @ResponseBody
    @ApiOperation("查询书名")
    public List<Book> getBookList(
            @ApiParam("书名") @RequestParam("key") String key,
            @ApiParam("数据源") @RequestParam("dataSource") String dataSource
    ) {
        try {
            if(StringUtils.isEmpty(key)){
                return null;
            }if(StringUtils.isEmpty(dataSource)){
                dataSource="ddxs";
            }
            long start = System.currentTimeMillis();
            List<Book> bookList = bookService.getBookList(key, dataSource);
            long end = System.currentTimeMillis();
            System.out.println("本次获取目录共用时" + (end - start) / 1000 + "s");
            return bookList;
        } catch (Exception e) {
            log.info("ERROR----url:book/getBookList;key=" + key + ";dataSource=" + dataSource);
            e.printStackTrace();
            return null;
        }

    }

    @PostMapping("/getBookCatalogue")
    @ResponseBody
    @ApiOperation("获取目录")
    public List<BookCatalogueDto> getBookCatalogue(
            @ApiParam("书籍编码") @RequestParam("bookCod") String bookCod,
            @ApiParam("数据源") @RequestParam("dataSource") String dataSource
    ) {
        try {
            if(StringUtils.isEmpty(bookCod)){
                return null;
            }if(StringUtils.isEmpty(dataSource)){
                dataSource="ddxs";
            }
            long start = System.currentTimeMillis();
            List<BookCatalogueDto> bookCatalogue = bookService.getBookCatalogue(bookCod, dataSource);
            long end = System.currentTimeMillis();
            System.out.println("本次获取目录共用时" + (end - start) / 1000 + "s");
            return bookCatalogue;
        } catch (Exception e) {
            log.info("ERROR----url:book/getBookCatalogue;bookCod=" + bookCod + ";dataSource=" + dataSource);
            e.printStackTrace();
            return null;
        }
    }

    @RequestMapping("/getBookFile")
    @ResponseBody
    @ApiOperation("获取当前有的书籍")
    public List<FileBook> getBookFile(
            @RequestParam(value = "page", required = false) Integer page
    ) {
        try {
            if (page == null) {
                page = 1;
            }
            return bookService.getBookFile(page);
        } catch (Exception e) {
            log.info("ERROR----url:book/getBookFile;page=" + page);
            e.printStackTrace();
            return null;
        }


    }

    @PostMapping("/getBookChapter")
    @ResponseBody
    @ApiOperation("获取内容")
    public BookCatalogueDto getBookChapter(
            @ApiParam("书籍编码") @RequestParam("bookCod") String bookCod,
            @ApiParam("章节编码") @RequestParam("chapterCod") String chapterCod,
            @ApiParam("数据源") @RequestParam("dataSource") String dataSource
    ) {
        try {
            if(StringUtils.isEmpty(bookCod)||StringUtils.isEmpty(chapterCod)){
                return null;
            }if(StringUtils.isEmpty(dataSource)){
                dataSource="ddxs";
            }
            return bookService.getBookChapter(bookCod, chapterCod, dataSource);
        } catch (Exception e) {
            log.info("ERROR----url:book/getBookChapter;bookCod=" + bookCod + ";chapterCod=" + chapterCod + ";dataSource=" + dataSource);
            e.printStackTrace();
            return null;
        }
    }

    @ResponseBody
    @RequestMapping("/downloadBook")
    @ApiOperation("下载书籍")
    public String downloadBook(
            @ApiParam("书籍编码") @RequestParam("bookCod") String bookCod,
            @ApiParam("书籍名称") @RequestParam("bookName1") String bookName,
            @ApiParam("数据源") @RequestParam("dataSource") String dataSource,
            HttpServletRequest request,
            HttpServletResponse response
    ) {
        try {
            if(StringUtils.isEmpty(bookCod)||StringUtils.isEmpty(bookName)){
                return null;
            }if(StringUtils.isEmpty(dataSource)){
                dataSource="ddxs";
            }
            long start = System.currentTimeMillis();
            bookService.downloadBook(bookCod, bookName, request, dataSource);
            long end = System.currentTimeMillis();
            System.out.println("本次下载共耗时" + (end - start) / 1000 + "s");
            download("/usr/local/webapps/file/" + bookName + ".txt", bookName + ".txt", request, response);
            return "";
        } catch (Exception e) {
            log.info("ERROR----url:book/downloadBook;bookCod=" + bookCod + ";bookName=" + bookName + ";dataSource=" + dataSource);
            e.printStackTrace();
            return "下载异常" + e;
        }
    }

}
