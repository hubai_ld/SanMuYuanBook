package com.aaa.entity;

import com.aaa.dto.BookCatalogueDto;

public class BookCatalogue {
    private Integer catalogueId;

    private Integer bookId;

    private Integer catalogueCode;

    private String catalogueName;

    private String catalogueText;

    /**
     * 下一章code
     */
    private Integer nextCode;

    /**
     * 上一章code
     */
    private Integer upCode;
    public BookCatalogue(){

    }

    public BookCatalogue(BookCatalogueDto bookCatalogueDto){
        this.catalogueId=bookCatalogueDto.getCatalogueId();
        this.catalogueCode=bookCatalogueDto.getCatalogueCod();
        this.nextCode=bookCatalogueDto.getNextCode();
        this.catalogueName=bookCatalogueDto.getCatalogueName();
        this.catalogueText=bookCatalogueDto.getCatalogueText();
    }

    public Integer getNextCode() {
        return nextCode;
    }

    public void setNextCode(Integer nextCode) {
        this.nextCode = nextCode;
    }

    public Integer getCatalogueId() {
        return catalogueId;
    }

    public void setCatalogueId(Integer catalogueId) {
        this.catalogueId = catalogueId;
    }

    public Integer getBookId() {
        return bookId;
    }

    public void setBookId(Integer bookId) {
        this.bookId = bookId;
    }

    public Integer getCatalogueCode() {
        return catalogueCode;
    }

    public void setCatalogueCode(Integer catalogueCode) {
        this.catalogueCode = catalogueCode;
    }

    public String getCatalogueName() {
        return catalogueName;
    }

    public void setCatalogueName(String catalogueName) {
        this.catalogueName = catalogueName;
    }

    public String getCatalogueText() {
        return catalogueText;
    }

    public void setCatalogueText(String catalogueText) {
        this.catalogueText = catalogueText;
    }
}