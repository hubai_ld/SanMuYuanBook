package com.aaa.entity;

/**
 * @author 杨森
 * @version 1.0
 * @Title: 漫画类
 * @date 2020/8/24 13:06
 */
public class Cartoon {
    private String cName;
    private String cImgSrc;
    private String cAuthor;
    private String cCode;

    public String getcCode() {
        return cCode;
    }

    public void setcCode(String cCode) {
        this.cCode = cCode;
    }

    public String getcName() {
        return cName;
    }

    public void setcName(String cName) {
        this.cName = cName;
    }

    public String getcImgSrc() {
        return cImgSrc;
    }

    public void setcImgSrc(String cImgSrc) {
        this.cImgSrc = cImgSrc;
    }

    public String getcAuthor() {
        return cAuthor;
    }

    public void setcAuthor(String cAuthor) {
        this.cAuthor = cAuthor;
    }
}
