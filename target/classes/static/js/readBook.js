(function ($) {
    $.getUrlParam = function (name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
        var r = window.location.search.substr(1).match(reg);
        if (r != null) return unescape(r[2]); return null;
    }
})(jQuery);
$(function () {
    $.ajax({
        type: "POST",
        url: "/book/getBookChapter",
        data: {
            "bookCod": $.getUrlParam('bookCod'),
            "chapterCod": $.getUrlParam('chapterCod')
        },
        dataType: "json",
        success: function (data) {
            $("#body").append(data.catalogueText);
            $("#head").append(data.catalogueName);
            if(data.upCode!=null){
                 $("#j_chapterPrev").attr("href","readBook.html?bookCod="+data.bookCode+"&&chapterCod="+data.upCode);
            }
            if(data.nextCode!=null){
                $("#j_chapterNext").attr("href","readBook.html?bookCod="+data.bookCode+"&&chapterCod="+data.nextCode);
            }
        },
        complete: function (data) {

        }
    });
    $("#j_fontSize span").click(function(){
        var thisEle = $("#body").css("font-size");
        var textFontSize = parseFloat(thisEle , 10);
        var unit = thisEle.slice(-2); //获取单位
        var cName = $(this).attr("id");
        if(cName == "bigger"){
            if( textFontSize <= 22 ){
                textFontSize += 1;
                $("#size").html(textFontSize);
            }
        }else if(cName == "smaller"){
            if( textFontSize >= 12 ){
                textFontSize -= 1;
                $("#size").html(textFontSize);
            }
        }

        $("#body").css("font-size", textFontSize + unit);
    });
    $(".back").click(function () {
        window.location.href="BookCatalogue.html?bookCod="+$.getUrlParam('bookCod');
    })
    $("#shezhi").click(function () {
        $("#j_setting").attr("style","display: block;");
    })
    $("#guanbi").click(function () {
        $("#j_setting").attr("style","display: none;");
    })
})